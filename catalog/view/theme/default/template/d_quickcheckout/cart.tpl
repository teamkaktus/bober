<!-- 
	Ajax Quick Checkout 
	v6.0.0
	Dreamvention.com 
	d_quickcheckout/cart.tpl 
-->
<div id="cart_view" class="qc-step" data-col="<?php echo $col; ?>" data-row="<?php echo $row; ?>"></div>
<script type="text/html" id="cart_template">

	<div class="panel panel-default <%= model.config.display ? '' : 'hidden' %>">
		<!--<div class="panel-heading">
			<h4 class="panel-title">
				<span class="icon">
					<i class="<%= model.config.icon %>"></i>
				</span>
				<span class="text"><%= model.config.title %> <%= (model.cart_weight) ? '('+model.cart_weight+')' : '' %></span>
			</h4>
		</div>-->

		<div class="qc-checkout-product panel-body ssss_ff" >
			<% if(model.error){ %>
				<div class="alert alert-danger">
					<i class="fa fa-exclamation-circle"></i> <%= model.error %>
				</div>
			<% } %>

			<table class="table table-bordered qc-cart">
				<div id="table_cart" class="table_cart">
					<div class="cart_table_row hidden-sm hidden-xs">
						<div class="table_cell1 <%= parseInt(model.config.columns.image) ? '' : 'hidden' %>">Изображение:</div>
						<div class="table_cell1 <%= parseInt(model.config.columns.name) ? '' : 'hidden' %>">Название товара:</div>
						<div class="table_cell1 hidden-xs <%= parseInt(model.config.columns.price) && model.show_price ? '' : 'hidden' %>"><?php echo $column_price; ?>:</div>
						<div class="table_cell1 <%= parseInt(model.config.columns.quantity) ? '' : 'hidden' %>"><?php echo $column_quantity; ?>:</div>
						<div class="table_cell1 <%= parseInt(model.config.columns.total) && model.show_price ? '' : 'hidden' %>"><?php echo $column_total; ?>:</div>
						<div class="table_cell1">.</div>
					</div>
				</div>

				<tbody>
					<% _.each(model.products, function(product) { %>
					<div id="table_cart" class="table_cart">
					<div class="cart_table_row clearfix clea_bord_top">
						<div class="qc-image <%= parseInt(model.config.columns.image) ? '' : 'hidden' %> table_cell">
							<a  href="<%= product.href %>" data-container="body" data-toggle="popover" data-placement="top" data-content='<img src="<%= product.image %>" />' data-trigger="hover">
								<img src="<%= product.thumb %>" class="img-responsive"/>
							</a>
						</div>

						<div class="qc-name <%= parseInt(model.config.columns.name) ? '' : 'hidden' %> table_cell"><div class="number">
							<a href="<%= product.href %>" <%=  model.config.columns.image ? '' : 'rel="popup" data-help=\'<img src="' + product.image + '"/>\'' %>>
								<%= product.name %> <%= product.stock ? '' : '<span class="out-of-stock">***</span>' %>
							</a>
							<% _.each(product.option, function(option) { %>
								<div> &nbsp;<small> - <%= option.name %>: <%= option.value %></small> </div>
							<% }) %>
							<% if(parseInt(model.config.columns.model)){ %>
								<div class="qc-name-model visible-xs-block"><small><span class="title"><?php echo $column_model; ?>:</span> <span class="text"><%= product.model %></span></small></div>
							<% } %>
							<!--<% if(parseInt(model.config.columns.price) && model.show_price){ %>
								<div class="qc-name-price visible-xs-block "><small><span class="title"><?php echo $column_price; ?>:</span> <span class="text"><%= product.price %></span></small></div>
							<% } %>
							<% if (product.reward) { %>
								<div><small><%= product.reward %></small></div>
							<% } %>-->
							<% if (product.recurring) { %>
								<div><span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><%= product.recurring %></small></div>
							<% } %>
						</div></div>
						<div class="qc-price <%= parseInt(model.config.columns.price) && model.show_price  ? '' : 'hidden' %> table_cell"><div class="number"><%= product.price %></div></div>

						<div class="<%= parseInt(model.config.columns.quantity) ? '' : 'hidden' %> table_cell"><div class="number">
							<div class="input-group input-group-sm">
								<span class="input-group-btn">
									<button class=" decrease minus" data-product="<%= product.key %>"></button>
								</span>
								<input type="text" data-mask="9?999999999999999" value="<%= product.quantity %>" class="qc-product-qantity form-control text-center" name="cart.<%= product.key %>"  data-refresh="2"/>
								<span class="input-group-btn">
									<button class="increase plus" data-product="<%= product.key %>"></button>
								</span>
							</div>
						</div>
						</div>

						<div class="qc-total <%= parseInt(model.config.columns.total) && model.show_price  ? '' : 'hidden' %> table_cell"><div class="number"><%= product.total %></div></div>
						<div class="<%= parseInt(model.config.columns.quantity) ? '' : 'hidden' %> table_cell"><div class="number">
								<div class="input-group input-group-sm">
								<span class="input-group-btn hidden-lg hidden-md hidden-sm hidden-xs" >
									<button class="btn btn-primary decrease hidden-md hidden-sm hidden-xs" data-product="<%= product.key %>"><i class="fa fa-chevron-down hidden-xs"></i></button>
								</span>
									<input type="text" data-mask="9?999999999999999" value="<%= product.quantity %>" class="qc-product-qantity form-control text-center hidden-lg hidden-md hidden-sm hidden-xs" name="cart.<%= product.key %>"  data-refresh="2"/>
									<span class="input-group-btn">
									<button class="btn btn-primary increase hidden-md hidden-lg hidden-sm hidden-xs" data-product="<%= product.key %>"><i class="fa fa-chevron-up hidden-xs"></i></button>

									<button class="delete bt_back_22" data-product="<%= product.key %>"></button>
								</span>
								</div>
						</div>
						</div>
					</div>
					<% }) %>
					<% _.each(model.vouchers, function(voucher) { %>
			        <tr>
			          <td class="qc-image <%= parseInt(model.config.columns.image) ? '' : 'hidden' %> "></td>
			          <td class="qc-name <%= parseInt(model.config.columns.name) ? '' : 'hidden' %> "><%= voucher.description %></td>
			          <td class="qc-model <%= parseInt(model.config.columns.model) ? '' : 'hidden' %> "></td>
			          <td class="qc-quantity <%= parseInt(model.config.columns.quantity) ? '' : 'hidden' %> ">1</td>
			          <td class="qc-price <%= parseInt(model.config.columns.price) && model.show_price ? '' : 'hidden' %> "><%= voucher.amount %></td>
			          <td class="qc-total <%= parseInt(model.config.columns.total) && model.show_price ? '' : 'hidden' %> "><%= voucher.amount %></td>
			        </tr>
			        <% }) %>

				</tbody>
			</table>
			<div class="floa_rig_22"><span class="spa_font_1">Итого:</span>
				<span class="">
				<% if(model.show_price){ %>
				<span class="form-horizontal qc-totals">
					<span><%= model.totals[0].text %></span>
				</span>
				<% } %>
				</span>
			</div>
			<!--<% _.each(model.products, function(product) { %>
							<div class="qc-total <%= parseInt(model.config.columns.total) && model.show_price  ? '' : 'hidden' %> table_cell1">ИТОГО:<span class="spa_font_1"><%= product.total %></span></div>
						<% }) %>-->
			<div class="form-horizontal">
				<div class=" form-group qc-coupon <%= parseInt(model.config.option.coupon.display) ? '' : 'hidden' %>">
					<% if(model.errors.coupon){ %>
						<div class="col-sm-12">
							<div class="alert alert-danger">
								<i class="fa fa-exclamation-circle"></i> <%= model.errors.coupon %>
							</div>
						</div>
					<% } %>
					<% if(model.successes.coupon){ %>
						<div class="col-sm-12">
							<div class="alert alert-success">
								<i class="fa fa-exclamation-circle"></i> <%= model.successes.coupon %>
							</div>
						</div>
					<% } %>
					<label class="col-sm-4 control-label" >
						<?php echo $text_use_coupon; ?>
					</label>
					<div class="col-sm-8">
						<div class="input-group">
							<input type="text" value="<%= model.coupon ? model.coupon : '' %>" name="coupon" id="coupon" placeholder="<?php echo $text_use_coupon; ?>" class="form-control"/>
							<span class="input-group-btn">
								<button class="btn btn-primary" id="confirm_coupon" type="button"><i class="fa fa-check"></i></button>
							</span>
						</div>
					</div>
					<% _.each(model.coupon, function(voucher) { %>
			        
			        <% }) %>
				</div>
				<div class=" form-group qc-voucher <%= parseInt(model.config.option.voucher.display) ? '' : 'hidden' %>">
					<% if(model.errors.voucher){ %>
						<div class="col-sm-12">
							<div class="alert alert-danger">
								<i class="fa fa-exclamation-circle"></i> <%= model.errors.voucher %>
							</div>
						</div>
					<% } %>
					<% if(model.successes.voucher){ %>
						<div class="col-sm-12">
							<div class="alert alert-success">
								<i class="fa fa-exclamation-circle"></i> <%= model.successes.voucher %>
							</div>
						</div>
					<% } %>

					<label class="col-sm-4 control-label" >
						<?php echo $text_use_voucher; ?>
					</label>
					<div class="col-sm-8">
						<div class="input-group">
							<input type="text" value="<%= model.voucher ? model.voucher : '' %>" name="voucher" id="voucher" placeholder="<?php echo $text_use_voucher; ?>" class="form-control"/>
							<span class="input-group-btn">
								<button class="btn btn-primary" id="confirm_voucher" type="button"><i class="fa fa-check"></i></button>
							</span>
						</div>
					</div>
				</div>
				<?php if($reward_points) {?>
				<div class=" form-group qc-reward <%= parseInt(model.config.option.reward.display) ? '' : 'hidden' %>">
					<% if(model.errors.reward){ %>
						<div class="col-sm-12">
							<div class="alert alert-danger">
								<i class="fa fa-exclamation-circle"></i> <%= model.errors.reward %>
							</div>
						</div>
					<% } %>
					<% if(model.successes.reward){ %>
						<div class="col-sm-12">
							<div class="alert alert-success">
								<i class="fa fa-exclamation-circle"></i> <%= model.successes.reward %>
							</div>
						</div>
					<% } %>
					<label class="col-sm-4 control-label" >
						<?php echo $text_use_reward; ?>
					</label>
					<div class="col-sm-8">
						<div class="input-group">
							<input type="text" value="<%= model.reward ? model.reward : '' %>" name="reward" id="reward" placeholder="<?php echo $text_use_reward; ?>" class="form-control"/>
							<span class="input-group-btn">
								<button class="btn btn-primary" id="confirm_reward" type="button"><i class="fa fa-check"></i></button>
							</span>

						</div>
						<small><?php echo $entry_reward; ?></small>
					</div>

				</div>
				<?php } ?>
			</div>

			<div class="preloader row"><img class="icon" src="image/<%= config.general.loader %>" /></div>
		
		</div>
	</div>

</script>
<script>
$(function() {
	qc.cart = $.extend(true, {}, new qc.Cart(<?php echo $json; ?>));
	qc.cartView = $.extend(true, {}, new qc.CartView({
		el:$("#cart_view"), 
		model: qc.cart, 
		template: _.template($("#cart_template").html())
	}));

});

</script>