<?php echo $header; ?>
<div class="container">
  <section id="contacts">
    <h1 class="hidden-sm hidden-xs">Корзина</h1>
  </section>
  <?php if ($attention) { ?>
  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <!--<h1><?php echo $heading_title; ?>
        <?php if ($weight) { ?>
        &nbsp;(<?php echo $weight; ?>)
        <?php } ?>
      </h1>-->
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="table-responsive">
          <table class="table table-bordered">
            <div id="table_cart" class="table_cart">
              <div class="cart_table_row hidden-xs hidden-sm">
                  <div class="table_cell1"><?php echo $column_image; ?></div>
                  <div class="table_cell1"><?php echo $column_name; ?></div>
                  <div class="table_cell1"><?php echo $column_quantity; ?></div>
                  <div class="table_cell1"><?php echo $column_price; ?></div>
                  <div class="table_cell1"><?php echo $column_total; ?></div>
                <div class="table_cell1">
                  <span>Итого: <span id="allTotalPrice"><?php echo $totals['0']['text']; ?></span></span>
                </div>
               </div>
            </div>
            <tbody>
              <?php foreach ($products as $product) { ?>
              <div id="table_cart" class="table_cart">
                <div class="cart_table_row clearfix clea_bord_top">
                  <div class="table_cell">
                    <?php if ($product['thumb']) { ?>
                    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                    <?php } ?></div>
                  <div class="table_cell"><div class="number"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                    <?php if (!$product['stock']) { ?>
                    <span class="text-danger">***</span>
                    <?php } ?>
                    <?php if ($product['option']) { ?>
                    <?php foreach ($product['option'] as $option) { ?>
                    <br />
                    <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                    <?php } ?>
                    <?php } ?>
                    <?php if ($product['reward']) { ?>
                    <br />
                    <small><?php echo $product['reward']; ?></small>
                    <?php } ?>
                    <?php if ($product['recurring']) { ?>
                    <br />
                    <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                    <?php } ?></div></div>
                  <div class="table_cell"><div class="number">
                      <div class="number">
                        <span class="minus" onclick="quantity_dec('<?=$product['cart_id'];?>', <?=$product['product_id'];?>)"></span>
                        <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]"
                               value="<?php echo $product['quantity']; ?>" size="5" oninput="quantity_onchange('<?=$product['cart_id'];?>', <?=$product['product_id'];?>)"/>
                        <span class="plus" onclick="quantity_inc('<?=$product['cart_id'];?>', <?=$product['product_id'];?>)"></span>
                      </div>
                  </div>
                  </div>
                  <div class="table_cell"><div class="number"><?php echo $product['price']; ?></div></div>
                  <div class="table_cell"><div class="number"><?php echo $product['total']; ?></div></div>
                  <div class="table_cell"><div class="number">
                    <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="bt_back_22" onclick="cart.remove('<?php echo $product['cart_id']; ?>');">
                    </button>
                  </div>
                  </div>
                </div>
              </div>
              <?php } ?>
              <?php foreach ($vouchers as $vouchers) { ?>
              <tr>
                <td></td>
                <td class="text-left"><?php echo $vouchers['description']; ?></td>
                <td class="text-left"></td>
                <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                    <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                    <span class="input-group-btn"><button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-times-circle"></i></button></span></div></td>
                <td class="text-right"><?php echo $vouchers['amount']; ?></td>
                <td class="text-right"><?php echo $vouchers['amount']; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </form>
      <?php if ($coupon || $voucher || $reward || $shipping) { ?>
      <h2><?php echo $text_next; ?></h2>
      <p><?php echo $text_next_choice; ?></p>
      <div class="panel-group" id="accordion"><?php echo $coupon; ?><?php echo $voucher; ?><?php echo $reward; ?><?php echo $shipping; ?></div>
      <?php } ?>
      <br />
      <div class="buttons">
        <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_shopping; ?></a></div>
        <div class="pull-right"><a href="<?php echo $checkout; ?>" class="btn btn-primary"><?php echo $button_checkout; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
