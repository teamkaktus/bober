<?php echo $header; ?>
<div class="container">
  <div class="row hidden-xs hidden-sm">
    <div class="col-md-3 span_text_kat1">
      <span class="span_text_katalog"><?php echo $heading_title; ?></span>
    </div>
    <div class="col-md-9 col_padd_16">
      <ul class="breadcrumbs hidden-xs  col-sm-12 col-md-12 col-lg-12">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if ($breadcrumb ['href']==('')){ ?>
        <li class="breadcrumb_text"><?php echo $breadcrumb['text']; ?></li>
        <?php }else{ ?>
        <li class="breadcrumb_text">
          <a class="text" href="<?php echo $breadcrumb['href']; ?>"
             style="padding-right: 12px; background: url('../../../catalog/view/theme/default/image/2.png') right center no-repeat;"><?php echo $breadcrumb['text']; ?></a>
        </li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content conten" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php echo $description; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>