<?php echo $header; ?>
    <div class="container">
    <section id="contacts">
        <h1 class="hidden-sm hidden-xs">Контакты</h1>
        <div class="col-md-9 col_padd_16">
            <ul class="breadcrumbs hidden-xs  col-sm-12 col-md-12 col-lg-12">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php if ($breadcrumb ['href']==('')){ ?>
                <li class="breadcrumb_text"><?php echo $breadcrumb['text']; ?></li>
                <?php }else{ ?>
                <li class="breadcrumb_text">
                    <a class="text" href="<?php echo $breadcrumb['href']; ?>"
                       style="padding-right: 12px; background: url('../../../catalog/view/theme/default/image/2.png') right center no-repeat;"><?php echo $breadcrumb['text']; ?></a>
                </li>
                <?php } ?>
                <?php } ?>
            </ul>
        </div>
        </section>
      <section id="contacts">
          <div class="row">
            <div class="clearfix"></div>

            <div class="cont-wrapper">
                <h1 class="hidden-lg hidden-md">Контакты</h1>
              <div class="text-row text_rows_np">
                  <div class="cont" style="margin-bottom: 0;	">
                      <img src="/image/icon_adres.png" alt="">
                      <div class="text_style_1 contact-text-title-style1">Адрес:</div>
                      <div class="text_style_2 contact-text-style1"><?php echo $address; ?></div>
                  </div>
                <div class="cont">
                  <img src="/image/icon_email.png" alt="">
                  <div class="text_style_1 contact-text-title-style">Email:</div>
                  <div class="text_style_2 contact-text-style"><?php echo $config_email; ?></div>
                </div>
                  <div class="border_cotntact hidden-xs hidden-sm"></div>
                <div class="cont">
                  <img src="/image/icon_telephone.png" alt="">
                  <div class="text_style_1 contact-text-title-style">Телефон:</div>
                  <div class="text_style_2 contact-text-style"><?php echo $telephone; ?> / <?php echo $telephone_2; ?></div>
                </div>
                  <div class="border_cotntact hidden-xs hidden-sm"></div>
                <div class="cont">
                  <img src="/image/icon_open.png" alt="">
                  <div class="text_style_1 contact-text-title-style">График работы:</div>
                  <div class="text_style_2 contact-text-style"><?php echo $open; ?></div>
                </div>
                  <div class="border_cotntact hidden-xs hidden-sm"></div>
              </div>
                <div class="text-row_style no-padding hidden-xs hidden-sm"></div>
            </div>
          </div>

        <div class="divider visible-xs visible-sm"></div>
        <div class="form contact_form_style hidden-sm hidden-xs">
          <span class="title_contact_form">есть вопрос?</span>

          <form id="contact_c">
            <ul id="errorMasege" style="padding-left: 0">
              <span style="display: none; color: red" class="row mail">Некоректно заполнено E-MAIL</span>
              <span style="display: none; color: red" class="row text">Некоректно заполненно поле вопрос</span>
            </ul>
            <label for="mail">Ваша электронная почта:</label>
            <input id="mail" name="mail" type="text">
            <label for="text">Ваш вопрос:</label>
            <textarea id="text" name="text"></textarea>
            <input id="send_c" type="submit">
          </form>
        </div>
          <div id="map" class="map_box ymaps fr clf "> </div>
          <div class="form contact_form_style hidden-lg hidden-md">
              <span class="title_contact_form">есть вопрос?</span>

              <form id="contact_c">
                  <ul id="errorMasege" style="padding-left: 0">
                      <span style="display: none; color: red" class="row mail">Некоректно заполнено E-MAIL</span>
                      <span style="display: none; color: red" class="row text">Некоректно заполненно поле вопрос</span>
                  </ul>
                  <label for="mail">Ваша электронная почта:</label>
                  <input id="mail" name="mail" type="text">
                  <label for="text">Ваш вопрос:</label>
                  <textarea id="text" name="text"></textarea>
                  <input id="send_c" type="submit">
              </form>
          </div>
      </section>
</div>
<div class="container">
<div class="divider-grey22">
</div>
</div>
<div class="container">
<section id="contacts">
    <div class="row">
        <div class="clearfix"></div>

        <div class="cont-wrapper">
            <div class="text-row text_rows_np">
                <div class="cont" style="margin-bottom: 0;	">
                    <img src="/image/icon_adres.png" alt="">
                    <div class="text_style_1 contact-text-title-style1">Адрес:</div>
                    <div class="text_style_2 contact-text-style1"><?php echo $address2; ?></div>
                </div>
                <div class="cont">
                    <img src="/image/icon_email.png" alt="">
                    <div class="text_style_1 contact-text-title-style">Email:</div>
                    <div class="text_style_2 contact-text-style"><?php echo $config_email; ?></div>
                </div>
                <div class="border_cotntact hidden-xs hidden-sm"></div>
                <div class="cont">
                    <img src="/image/icon_telephone.png" alt="">
                    <div class="text_style_1 contact-text-title-style">Телефон:</div>
                    <div class="text_style_2 contact-text-style"><?php echo $telephone; ?> / <?php echo $telephone_2; ?></div>
                </div>
                <div class="border_cotntact hidden-xs hidden-sm"></div>
                <div class="cont">
                    <img src="/image/icon_open.png" alt="">
                    <div class="text_style_1 contact-text-title-style">График работы:</div>
                    <div class="text_style_2 contact-text-style"><?php echo $open; ?></div>
                </div>
                <div class="border_cotntact hidden-xs hidden-sm"></div>
            </div>
            <div class="text-row_style no-padding hidden-xs hidden-sm"></div>
        </div>
    </div>

    <div class="divider visible-xs visible-sm"></div>
    <div class="form contact_form_style hidden-sm hidden-xs">
        <span class="title_contact_form">есть вопрос?</span>

        <form id="contact_c1">
            <ul id="errorMasege1" style="padding-left: 0">
                <span style="display: none; color: red" class="row mail1">Некоректно заполнено E-MAIL</span>
                <span style="display: none; color: red" class="row text1">Некоректно заполненно поле вопрос</span>
            </ul>
            <label for="mail1">Ваша электронная почта:</label>
            <input id="mail1" name="mail" type="text">
            <label for="text1">Ваш вопрос:</label>
            <textarea id="text1" name="text"></textarea>
            <input id="send_c1" type="submit">
        </form>
    </div>
    <div id="map1" class="map_box ymaps fr clf "> </div>
    <div class="form contact_form_style hidden-lg hidden-md">
        <span class="title_contact_form">есть вопрос?</span>

        <form id="contact_c1">
            <ul id="errorMasege" style="padding-left: 0">
                <span style="display: none; color: red" class="row mail1">Некоректно заполнено E-MAIL</span>
                <span style="display: none; color: red" class="row text1">Некоректно заполненно поле вопрос</span>
            </ul>
            <label for="mail1">Ваша электронная почта:</label>
            <input id="mail1" name="mail" type="text">
            <label for="text1">Ваш вопрос:</label>
            <textarea id="text1" name="text"></textarea>
            <input id="send_c1" type="submit">
        </form>
    </div>
</section>
</div>
<section id="contacts">
    <div class="service"><h2>Будем рады видеть Вас в числе наших клиентов!</h2></div>
</section>
      <?php echo $content_bottom; ?>

    <?php echo $column_right; ?>
<?php echo $footer; ?>
<script><!--
  $(document).ready(function () {

    $('#send_c').on('click', function (even) {
        even.preventDefault();
        if (!($('#mail').val().match(/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/i)) && ($('#text').val().length < 3)) {
            $('.mail').show();
            $('.text').show();
        } else if (!($('#mail').val().match(/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/i))) {
            $('.mail').show();
            $('.text').hide();
        } else if ($('#text').val().length < 10) {
            $('.text').show();
            $('.mail').hide();
        } else {
            var res = $('#contact_c').serializeArray();
            var arr = {}
                $.each(res, function (result) {
                    var $index = res[result].name;
                    arr[$index] = res[result].value;
                });
            $.ajax({
                url: 'index.php?route=information/contact/contactC',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });
            swal(
                'Повідомлення відправлено!',
                '',
                'success'
            );
            $('.mail').hide();
            $('.text').hide();
            $('#mail').css('border', '1px solid  #ccc');
            $('#mail').val('');
            $('#text').css('border', '1px solid  #ccc');
            $('#text').val('');
        }
    });

  });
--></script>
<script><!--
    $(document).ready(function () {

        $('#send_c1').on('click', function (even) {
            even.preventDefault();
            if (!($('#mail1').val().match(/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/i)) && ($('#text1').val().length < 3)) {
                $('.mail1').show();
                $('.text1').show();
            } else if (!($('#mail1').val().match(/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/i))) {
                $('.mail1').show();
                $('.text1').hide();
            } else if ($('#text1').val().length < 10) {
                $('.text1').show();
                $('.mail1').hide();
            } else {
                var res = $('#contact_c1').serializeArray();
                var arr = {}
                        $.each(res, function (result) {
                            var $index = res[result].name;
                            arr[$index] = res[result].value;
                        });
                $.ajax({
                    url: 'index.php?route=information/contact/contactC',
                    type: 'post',
                    dataType: 'json',
                    data: arr,
                    success: function () {
                    }
                });
                swal(
                        'Повідомлення відправлено!',
                        '',
                        'success'
                );
                $('.mail1').hide();
                $('.text1').hide();
                $('#mail1').css('border', '1px solid  #ccc');
                $('#mail1').val('');
                $('#text1').css('border', '1px solid  #ccc');
                $('#text1').val('');
            }
        });

    });
    --></script>
<script type="text/javascript">
    ymaps.ready(init);
    var myMap;
    function init () {
        myMap = new ymaps.Map("map", {
            center: [55.814927, 37.781524],
            behaviors: ['default', 'scrollZoom'],
            zoom: 18
        });
        myMap.controls

        myPlacemark0 = new ymaps.Placemark([55.814927, 37.781524], {
            balloonContent: ''
        });
        myMap.geoObjects
                .add(myPlacemark0);
    }
</script>
<script type="text/javascript">
    ymaps.ready(init);
    var myMap;
    function init () {
        myMap = new ymaps.Map("map1", {
            center: [59.887309, 30.487985],
            behaviors: ['default', 'scrollZoom'],
            zoom: 18
        });
        myMap.controls

        myPlacemark0 = new ymaps.Placemark([59.887309, 30.487985], {
            balloonContent: ''
        });
        myMap.geoObjects
                .add(myPlacemark0);
    }
</script>