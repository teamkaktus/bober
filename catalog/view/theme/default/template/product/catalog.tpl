<?php echo $header; ?>
<div class="container">
    <div class="row"><div class="col-lg-2 col-md-2 col-sm-4 col-xs-4"><?php echo $column_left; ?></div>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div class="row col-lg-10 col-md-10 col-sm-8 col-xs-8 marg_catal_bot"><?php echo $content_top; ?>
            <div class="row">
                <div class="breadcrumbs_catalog hidden-xs hidden-sm">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <?php if ($breadcrumb ['href']==('')){ ?>
                    <span class="icon_breadcrumbs"><?php echo $breadcrumb['text']; ?></span>
                    <?php }else{ ?>
                    <a class="breadcrumbs_catalog_style" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                    <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <?php foreach ($categories as $category) { ?>
            <?php if ($products[$category["category_id"]]) { ?>
                <div class="owl-carousel_catalog owl-theme">
                    <?php foreach ($products[$category["category_id"]] as $product) { ?>
                    <div class="items">
                        <div class="product-layout product-list col-xs-12">
                                <div class=""><a href="<?php echo $product['href']; ?>"><img
                                                src="<?php echo $product['thumb']; ?>"
                                                alt="<?php echo $product['name']; ?>"
                                                title="<?php echo $product['name']; ?>" class="img-responsive_catalog"/></a>
                                    <div class="caption_catalog_style">
                                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 no-padding pos_rel_bot_2">
                                            <div>
                                            <span class="catalog_name_style">
                                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        </span>
                                            </div>
                                            <div>
                                            <span class="catalog_manufacturer_style">
                                            <a href="<?php echo $product['href']; ?>"><?php echo $product['manufacturer']; ?></a>
                                        </span>
                                            </div>
                                        </div>
                                        <div class="price_catalog_style col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                            <p class="price">
                                                <?php if (!$product['special']) { ?>
                                                <span class="price-new"><?php echo $product['price']; ?> р</span>
                                                <?php } else { ?>
                                                <span class="price-new"><?php echo $product['special']; ?> р</span>
                                                <?php } ?>
                                                <?php if ($product['tax']) { ?>
                                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                <?php } ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
    <?php echo $column_right; ?>
    <?php echo $content_bottom; ?>
</div>

<script>
    $('.owl-carousel_catalog').owlCarousel_2({
        loop: true,
        nav: true,
        pagination: false,
        navText: ['<img src="/image/slider_strilka_2.png" />', '<img src="/image/slider_strylka.png" />'],
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 2
            },
            768: {
                items: 2
            },
            992: {
                items: 4
            },
            1200: {
                items: 5
            },
        }
    })
</script>
<?php echo $footer; ?>