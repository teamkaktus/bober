<?php echo $header; ?><?php if( ! empty( $mfilter_json ) ) { echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>'; } ?>
<div class="container">
  <div class="col-sm-12 mar_rel_22 hidden-sm hidden-xs">
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
    <ul class="breadcrumbs hidden-xs  col-sm-12 col-md-12 col-lg-12 poz_rel_22">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if ($breadcrumb ['href']==('')){ ?>
      <li class="breadcrumb_text"><?php echo $breadcrumb['text']; ?></li>
      <?php }else{ ?>
      <li class="breadcrumb_text">
        <a class="text" href="<?php echo $breadcrumb['href']; ?>"
           style="padding-right: 12px; background: url('../../../catalog/view/theme/default/image/2.png') right center no-repeat;"><?php echo $breadcrumb['text']; ?></a>
      </li>
      <?php } ?>
      <?php } ?>
    </ul>
  </div>
  </div>
  <div class="row">

    <div class="col-md-2 col-xs-4">
      <?php echo $column_right; ?>
    </div>

    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-lg-10 col-md-10 col-sm-8 col-xs-8'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-lg-10 col-md-10 col-sm-8 col-xs-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-lg-10 col-md-10 col-sm-8 col-xs-8'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><div id="mfilter-content-container">
      <div class="row">
        <div class="col-md-6 text-right">
        </div>
        <div class="col-md-3 text-right">
          <select id="input-sort" class="form-control form_control22_3" onchange="location = this.value;">

            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
      </div>
      <br />
      <div class="row row_2_2">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-list container_category_style col-lg-3 col-md-6 col-sm-6 col-xs-6">
          <div class="">
            <div class=""><a href="<?php echo $product['href']; ?>"><img
                        src="<?php echo $product['thumb']; ?>"
                        alt="<?php echo $product['name']; ?>"
                        title="<?php echo $product['name']; ?>" class="img-responsive_catalog"/></a>
              <div class="caption_catalog_style">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 no-padding">
                  <div>
                                            <span class="catalog_name_style">
                                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        </span>
                  </div>
                  <div>
                                            <span class="catalog_manufacturer_style">
                                            <a href="<?php echo $product['href']; ?>"><?php echo $product['manufacturer']; ?></a>
                                        </span>
                  </div>
                </div>
                <?php if ($product['rating']) { ?>
                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <?php } ?>
                <div class="price_catalog_style col-lg-5 col-md-5 col-sm-5 col-xs-5">
                  <p class="price">
                    <?php if (!$product['special']) { ?>
                    <span class="price-new"><?php echo $product['price']; ?></span>
                    <?php } else { ?>
                    <span class="price-new"><?php echo $product['special']; ?></span>
                    <?php } ?>
                    <?php if ($product['tax']) { ?>
                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                    <?php } ?>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left co_text_left"><?php echo $pagination; ?></div>
      </div>

      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
  </div>
</div>
</div>
  <?php echo $content_bottom; ?>
  </div>
<?php echo $footer; ?>
