<footer class="footer">
  <div class="sm-xs visible-xs visible-sm">
    <div class="phone-wrap">
      <span class="phone blue"><?php echo $telephone; ?></span>
      <span class="city">Москва</span>
    </div>
    <div class="phone-wrap">
      <span class="phone green"><?php echo $telephone_2; ?></span>
      <span class="city">Санкт-Петербург</span>
    </div>
    <div class="link-wrap">
      <a href="#">поделиться
        <img src="catalog\view\theme\default\img/vk.png" alt="">
        Вконтакте</a>
    </div>
    <div class="back-call-wrap">
      <a data-toggle="modal" data-target="#contactForm" class="back-call">Обратный звонок</a>
    </div>
    <div class="copyright-wrap">
      <p>Copyright 2016. Mebober.ru. Все права защищены</p>
    </div>
  </div>
  <div class="container foot hidden-xs hidden-sm">
    <div class="foot-menu KP-20">
      <span class="title">Каталог</span>
      <ul class="no-padding">
        <?php foreach ($categories as $category) { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <div class="foot-menu KP-20">
      <span class="title">Клиентам</span>
      <ul class="no-padding">
        <li><a href="index.php?route=information/information&information_id=6">Доставка и оплата</a></li>
        <li><a href="index.php?route=information/contact">Контакты</a></li>
        <li><a href="index.php?route=information/information&information_id=8">Гарантия</a></li>
        <li><a href="index.php?route=information/information&information_id=4">О нас</a></li>
        <li><a href="index.php?route=information/information&information_id=7">Акции</a></li>
      </ul>
    </div>
    <div class="foot-menu KP-20 style_graphick_roboty">
      <span class="title">Режим работы</span>
      <div class="p-wrapper">
        <div class="open"><?php echo $config_open; ?></div>
      </div>
      <div class="p-wrapper">
        <div class="open_2"><?php echo $config_open_2; ?></div>
      </div>
    </div>
    <div class="foot-menu KP-20">
      <span class="title">Телефоны</span>
      <div class="phone-wrapper">
        <span class="phone"><?php echo $telephone; ?></span>
        <p class="phone_text">Москва</p>
      </div>
      <div class="phone-wrapper">
        <span class="phone"><?php echo $telephone_2; ?></span>
        <p class="phone_text">Санкт-Петербург</p>
      </div>
      <div class="phone-wrapper hidden-xs hidden-sm text-left col-lg-5 col-md-5">
        <img class="icon_footer_style" src="/image/Copyright.png">
      </div>
    </div>
    <div class="foot-right KP-20">
      <div class="yarlik">
          <?php if(!empty($data['modules'][0])){ ?>
          <?php echo $data['modules'][0]; ?>
          <?php }?>
      </div>
      <span class="title">Соц.сеть</span>
      <div class="cont">
        <div class="a-wrap"><a href="#" class="social"></a></div>
        <a data-toggle="modal" data-target="#contactForm" class="back-call">Обратный звонок</a>
        <div class="logo-wrapper">
          <a href=""><img src="/image/logo_footer.png" class="img-responsive"/></a>
        </div>
      </div>
    </div>
  </div>
  <div class="container pos_rel_fott_2">
    <div class="cop footer_cop22 hidden-xs hidden-sm text-centr col-lg-12 col-md-12">
      <div class="copy">Copyright 2016. Mebober.ru. Все права защищены. </div>
    </div>
  </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>