<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
       <meta charset="UTF-8"/>
       <meta name="viewport" content="width=600, initial-scale=0.6">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <title><?php echo $title; ?></title>
       <base href="<?php echo $base; ?>"/>
       <?php if ($description) { ?>
       <meta name="description" content="<?php echo $description; ?>"/>
       <?php } ?>
       <?php if ($keywords) { ?>
       <meta name="keywords" content="<?php echo $keywords; ?>"/>
       <?php } ?>
       <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
       <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>

       <script src="catalog/view/javascript/mf/jquery-ui.min.js" type="text/javascript"></script>
       <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

       <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
       <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>
       <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
       <link href="catalog/view/theme/default/stylesheet/contactForm.css" rel="stylesheet">
       <link href="catalog/view/theme/default/stylesheet/headerCSS.css" rel="stylesheet">

       <link href="catalog/view/theme/default/stylesheet/main.min.css" rel="stylesheet">
       <link href="catalog/view/theme/default/stylesheet/fonts.min.css" rel="stylesheet">
       <link href="catalog/view/theme/default/stylesheet/contactCSS.css" rel="stylesheet">

        <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
               media="<?php echo $style['media']; ?>"/>
        <?php } ?>
        <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
        <script src="catalog/view/javascript/contactForm.js" type="text/javascript"></script>
        <script src="catalog/view/javascript/headerJS.js" type="text/javascript"></script>

        <script src="../catalog/view/javascript/auto-fix/js/jquery.autofix_anything.js" type="text/javascript"></script>
        <link href="../catalog/view/javascript/auto-fix/css/autofix_anything.css" rel="stylesheet">

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?139"></script>

    <script type="text/javascript">
        VK.init({apiId:5863144,onlyWidgets:true});
    </script>

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?139"></script>


    <script type="text/javascript" src="https://vk.com/js/api/share.js?94" charset="windows-1251"></script>
        <!--<script src="catalog/view/javascript/common_1.js" type="text/javascript"></script>
        <script src="catalog/view/javascript/libs.js" type="text/javascript"></script>
        <script src="catalog/view/javascript/libs.min.js" type="text/javascript"></script>-->
        <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>    <?php } ?>
        <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>" type="text/javascript"></script>
        <?php } ?>
        <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
        <?php } ?>
    </head>
<header class="header">
      <div class="container image_header_container_style_mob">
        <div class="row ">
            <div class="logo col-xs-9 col-sm-4 col-md-4 col-lg-2">
                <div id="logo">
                    <?php if ($logo) { ?>
                    <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>"
                                                        alt="<?php echo $name; ?>" class="img-responsive"/></a>
                    <?php } else { ?>
                    <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                    <?php } ?>
                </div>
            </div>
            <nav id="menu" class="navbar col-xs-3 hidden-sm hidden-md hidden-lg menu_pos_rel">
                <div class="navbar-header"><span id="category" class="visible-xs"></span>
                    <button type="button" class="navBTN open btn btn-navbar navbar-toggle no-padding" data-toggle="collapse"
                            data-target=".navbar-ex1-collapse"><img src="/image/btn-menu.png"></button>
                </div>
                <div id="navMenu" class="hidden-div-style">
                    <ul class="nav navbar-nav mb-navbar-nav-style">
                        <li class="mb-li-text-style-1"><a class="li-a-color" href="index.php?route=product/catalog">Каталог</a></li>
                        <li class="mb-li-text-style"><a class="li-a-color" href="index.php?route=information/information&information_id=4">О нас</a></li>
                        <li class="mb-li-text-style"><a class="li-a-color" href="index.php?route=information/information&information_id=7">Акции</a></li>
                        <li class="mb-li-text-style"><a class="li-a-color" href="index.php?route=information/information&information_id=8">Гарантия</a></li>
                        <li class="mb-li-text-style"><a class="li-a-color" href="index.php?route=information/information&information_id=6">Доставка и оплата</a></li>
                        <li class="mb-li-text-style-2"><a class="li-a-color" href="index.php?route=information/contact">Контакты</a></li>
                        <div style="clear: both"></div>
                    </ul>
                    <div class="back-call hidden-xs">
                        <a class="back-call-style" data-toggle="modal" data-target="#contactForm" >Обратный звонок</a>
                    </div>
                  
                </div>
            </nav>
            <div class="search-form-wrapper hidden-xs col-sm-8 col-md-8 col-lg-5">
                <span class="style_title_search">Поиск товара</span>
                <div class=""><?php echo $search; ?></div>
            </div>
            <div class="col-md-6 col-lg-5 no-padding">
                <div class="city-wrapper hidden-xs hidden-sm hidden-md col-lg-6 text-right no-padding">
                    <div class="telephone_style col-md-7 col-lg-8"><a href="<?php echo $contact; ?>"></a> <span
                                class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span>
                    </div>
                    <div class="msk hidden-xs hidden-sm hidden-md-5 col-lg-4 image_header_container_style">
                        <img class="image_header_style" src="/image/msk.png">
                    </div>
                    
                </div>
                <div class="city-wrapper hidden-xs hidden-sm hidden-md col-lg-6 text-right no-padding">
                    <div class="telephone_2_style col-md-7 col-lg-8"><a href="<?php echo $contact; ?>"></a> <span
                                class="hidden-xs hidden-sm hidden-md"><?php echo $telephone_2; ?></span>
                    </div>
                    <div class="spb hidden-xs hidden-sm hidden-md col-lg-4 image_header_container_style">
                        <img class="image_header_style" src="/image/spb.png">
                    </div>
                </div>
            </div>
        </div>
          </div>
    <div class="wrap_nav">
        <div class="container container_header_style ">
            <div class="search-form-wrapper col-xs-12 hidden-sm hidden-md hidden-lg">
                <span class="style_title_search hidden-xs">Поиск товара</span>
                <div class=""><?php echo $search; ?></div>
            </div>
            <nav id="menu" class="navbar_mob hidden-xs">
                <div class="navbar-header"><span id="category" class="visible-xs"></span>
                    <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
                </div>
                <div class=" collapse navbar-collapse navbar-ex1-collapse no-padding">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php?route=product/catalog">Каталог</a></li>
                        <li><a href="index.php?route=information/information&information_id=4">О нас</a></li>
                        <li><a href="index.php?route=information/information&information_id=7">Акции</a></li>
                        <li><a href="index.php?route=information/information&information_id=8">Гарантия</a></li>
                        <li><a href="index.php?route=information/information&information_id=6">Доставка и оплата</a></li>
                        <li><a href="index.php?route=information/contact">Контакты</a></li>
                    </ul>
                    <div class="back-call_stile hidden-xs hidden-sm hidden-md" >
                        <div class="style_graphick_icon">
                            <span class="mode">Режим работы</span>
                            <div>
                                <div class="p-wrapper mode_open"><?php echo $config_open; ?></div>
                            </div>
                        </div>
                        <div>
                            <a class="back-call-style" data-toggle="modal" data-target="#contactForm">Обратный звонок</a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <a class="baner_blok_1" href="#"></a>
        <a class="baner_blok_2" href="#"></a>
        <a class="baner_blok_3" href="#"></a>
        <a href="#stopblock" type="submit" class="btn_otzuvu hidden-xs clikblock">Оставить отзыв</a>
        <div class="hidden-xs">
            <?php if(!empty($data['modules'][0])){ ?>
            <?php echo $data['modules'][0]; ?>
            <?php }?>
        </div>
    </div>
    <div class="hidden-sm hidden-md hidden-lg">
        <?php if(!empty($data['modules'][1])){ ?>
        <?php echo $data['modules'][1]; ?>
        <?php }?>
    </div>
    </header>
<div class="fix_top">
    <div class="container container_style_category_home_1 hidden-sm hidden-xs">    <?= $cart?> </div>
    <div class=" hidden-lg hidden-md"><?= $cart?></div>
</div>
<div id="contactForm" class="modal fade">
        <div class="modal-dialog">
        <div class="modal-content modal-div-style">
            <div class="modal-header" style="border: 0">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="text-align: center;">
                <div class="modal-title-style">Обратная связь</div>
                <div class="form-width-style">
                    <form id="contactForm_c" action="" method="post">
                        <ul id="errorMasege" style="padding-left: 0">
                            <!--<span style="display: none; color: red" class="row name">Некоректно заполнено Имя пользователя</span>-->
                            <span style="display: none; color: red" class="row phone">Некоректно заполнен номер телефона</span>
                        </ul>
                        <div class="div-width-100">
                            <div class="div-width-100 input-title-style">
                                <span>Ваше имя:</span>
                            </div>
                            <input class="input-style contactItem text" id="name" name="name" type="text">
                        </div>
                        <div class="div-width-100 ">
                            <div class="div-width-100 input-title-style-2">
                                <span>Ваш телефон *:</span>
                            </div>
                            <input class="input-style contactItem text" id="phone" name="phone" type="text">
                        </div>
                        <div class="div-width-100 modal-btn-div-style">
                            <div class="modal-btn-img-style">
                                <button id="send" type="button" class="btn modal-btn-style">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>