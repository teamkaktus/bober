<div style="clear: left"></div><div class="container_catalog_style"><span class="text_catalog_style">Каталог</span></div>
<div class="col-md-12 cat-items-wrapper">
    <?php foreach ($categories as $category) { ?>
    <?php if ($category['category_id'] == $category_id) { ?>
      <div class="cat-item active">
        <a href="<?php echo $category['href']; ?>">
          <img src="<?php echo $category['image'];?>" class="img-responsive" alt="" data-hover="<?php echo $category['image2'];?>">
          <span class="cat-descr"><?php echo $category['name']; ?></span>
        </a>
      </div>
    <!--<?php if ($category['children']) { ?>
    <?php foreach ($category['children'] as $child) { ?>
    <?php if ($child['category_id'] == $child_id) { ?>
    <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
    <?php } else { ?>
    <a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
    <?php } ?>
    <?php } ?>
    <?php } ?>-->
    <?php } else { ?>
  <div class="cat-item">
    <a href="<?php echo $category['href']; ?>">
      <img src="<?php echo $category['image'];?>" class="img-responsive"alt="" data-hover="<?php echo $category['image2'];?>">
      <span class="cat-descr"><?php echo $category['name']; ?></span>
    </a>
  </div>
    <?php } ?>
    <?php } ?>
</div>
<div style="clear: left"></div>
<script>
  $('.cat-item').hover(function () {
    $(this).find('img[data-hover]').attr('tmp', $(this).find('img[data-hover]').attr('src')).attr('src', $(this).find('img[data-hover]').attr('data-hover')).attr('data-hover', $(this).find('img[data-hover]').attr('tmp')).removeAttr('tmp');

  }).each(function () {
    $('<img />').attr('src', $(this).find('img[data-hover]').attr('data-hover'));
  });
</script>