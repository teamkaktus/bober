<div class="container no-padding cont_mar_top">
    <div class="owl-carousel_category_home owl-theme cat-items-wrapper_home no-padding hei_cat_60">
            <?php foreach ($categories as $category) { ?>
            <?php if ($category['category_id'] == $category_id) { ?>
                <div class="new-item cat-item active">
                    <a href="<?php echo $category['href']; ?>">
                        <img style="width: 50%" src="<?php echo $category['image'];?>" class="img-responsive">
                        <span class="cat-descr"><?php echo $category['name']; ?></span>
                    </a>
                </div>
                <!--<?php if ($category['children']) { ?>
                <?php foreach ($category['children'] as $child) { ?>
                <?php if ($child['category_id'] == $child_id) { ?>
                <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
                <?php } else { ?>
                <a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
                <?php } ?>
                <?php } ?>
                <?php } ?>-->
                <?php } else { ?>
                <div class="cat-item hei_cat_60">
                    <a href="<?php echo $category['href']; ?>">
                        <img style="width: 50%" src="<?php echo $category['image'];?>" class="img-responsive" alt=""
                             data-hover="<?php echo $category['image2'];?>">
                        <span class="cat-descr"><?php echo $category['name']; ?></span>
                    </a>
                </div>
            <?php } ?>
            <?php } ?>
    </div>
</div>
<div class="container container_style_category_home hidden-sm hidden-xs"></div>
<script>
    $('.cat-item').hover(function () {
        $(this).find('img[data-hover]').attr('tmp', $(this).find('img[data-hover]').attr('src')).attr('src', $(this).find('img[data-hover]').attr('data-hover')).attr('data-hover', $(this).find('img[data-hover]').attr('tmp')).removeAttr('tmp');

    }).each(function () {
        $('<img />').attr('src', $(this).find('img[data-hover]').attr('data-hover'));
    });
</script>
<script>
    $('.owl-carousel_category_home').owlCarousel_1({
        loop: true,
        nav: true,
        pagination: false,
        margin: 30,
        navText: ['<img src="/image/strylka_vlivo_latest.png" />', '<img src="/image/strylka_vpravo_latest.png" />'],
        responsive: {
            0: {
                items: 3
            },
            600: {
                items: 3
            },
            768: {
                items: 6
            },
            1000: {
                items: 6
            }
        }
    })
</script>
