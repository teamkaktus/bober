<div class="container_KP">
    <div class="flexslider row">
        <ul class="slides">
            <?php foreach ($products as $product) { ?>
            <li class="col-lg-3" data-thumb="<?php echo $product['thumb']; ?>">
                <div class="product-layout ">

                    <div class="container_KP col-lg-12">
                        <div class="image col-lg-8">
                            <ul class="thumbnails">
                                <li><a class="thumbnail thumb" href="<?php echo $product['href']; ?>"
                                       title="<?php echo $heading_title; ?>"> <img id="target<?php echo $product['product_id']; ?>"
                                                src="<?php echo $product['thumb']; ?>"
                                                alt="<?php echo $product['name']; ?>"
                                                title="<?php echo $product['name']; ?>" class="img-responsive"/></a>
                                </li>
                            </ul>
                           <!-- <div class="top_tovar"><img src="/image/icon_top_tovar.png">
                                <div class="price-wrap_featured">
                                    <?php if (!$product['special']) { ?>
                                    <?php echo $product['price']; ?> руб
                                    <?php } else { ?>
                                    <span class="price"><?php echo $product['special']; ?> руб</span>
                                    <?php } ?>
                                    <?php if ($product['tax']) { ?>
                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                    <?php } ?>
                                </div>
                            </div>-->
                        </div>
                        <div class="str_pad col-lg-4 no-padding">
                            <div class="title-wrapper title_wrapper3333">
                                <!-- <p><?php echo $product['description']; ?></p> -->
                                <div class="feature_name_style">
                                <span class="catalog_name_style">
                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                </span>
                                    <span class="old-price_1 old_price_22">
                                        <?php if (!$product['special']) { ?>
                                        <?php echo $product['price']; ?> руб
                                        <?php } else { ?>
                                        <span class="price"><?php echo $product['special']; ?> руб</span>
                                        <?php } ?>
                                        <?php if ($product['tax']) { ?>
                                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                        <?php } ?>
                                    </span>
                                </div>
                                <div>
                                <span class="feature_manufacturer_style">
                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['manufacturer']; ?></a>
                                </span>
                                </div>
                                <div class="divider1"></div>
                                <div class="sizes-wrap_featured">
                                    <span class="sizes-title_featured">Размеры</span>
                                    <div class="size_featured">
                                        <div class="len_featured">
                                            <span>длина</span>
                                            <span class="count_featured"><?php echo $product['length']; ?> см</span>
                                        </div>
                                        <div class="crest_featured">X</div>
                                        <div class="len_featured">
                                            <span>ширина</span>
                                            <span class="count_featured"><?php echo $product['width']; ?> см</span>
                                        </div>
                                        <div class="crest_featured">X</div>
                                        <div class="len_featured">
                                            <span>высота</span>
                                            <span class="count_featured"><?php echo $product['height']; ?> см</span>
                                        </div>
                                    </div>
                                </div>

                                    <button  for="<?php echo $product['product_id']; ?>" type="button" id="button-cart" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary btn-lg btn-block bt_button_stal to-cart"><?php echo $button_cart; ?></button>
                            </div>

                        </div>

                    </div>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<script>
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    });
</script>

