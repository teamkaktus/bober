<div class="container">
    <div class="text-center text_center_23_2">
        <span class="latest_name_style text-center"><?php echo $name; ?></span>
    </div>
    <div>
        <p class="latest_text_style visible-md visible-lg""><?php echo $text; ?></p>
    </div>
    <section id="new">
        <div class="new-slider-wrapper">
            <div class="new-slider">
                <div class="owl-carousel_latest_home owl-theme">
                    <?php foreach ($products as $product) { ?>
                    <div class="new-item">
                        <div class="product-wrapper">
                            <?php if(!$product['icon'] == ''){ ?>
                            <div class="icon_latest_style"><img src="<?php echo $product['icon']; ?>" alt=""></div>
                            <?php } ?>
                            <img src="<?php echo $product['thumb']; ?>" class="img-responsive" alt="">
                            <div class="title-wrap">
                                <div class="title">
                                    <h3>Еврокровать</h3>
                                    <h2><?php echo $product['name']; ?></h2>
                                </div>
                            </div>
                            <div class="price-wrap">
                                <?php if ($product['price']) { ?>
                                <?php if (!$product['special']) { ?>
                                <span class="price"><?php echo $product['price']; ?></span>
                                <?php } else { ?>
                                <span class="price"><?php echo $product['special']; ?></span> <span
                                        class="old-price"><?php echo $product['price']; ?></span>
                                <?php } ?>
                                <?php if ($product['tax']) { ?>
                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <div class="sizes-wrap">
                                <span class="sizes-title">Размеры</span>
                                <div class="size">
                                    <div class="len">
                                        <span>длина</span>
                                        <span class="count"><?php echo $product['length']; ?> см</span>
                                    </div>
                                    <div class="crest">X</div>
                                    <div class="len">
                                        <span>ширина</span>
                                        <span class="count"><?php echo $product['width']; ?> см</span>
                                    </div>
                                    <div class="crest">X</div>
                                    <div class="len">
                                        <span>высота</span>
                                        <span class="count"><?php echo $product['height']; ?> см</span>
                                    </div>
                                </div>
                            </div>
                            <div class="link-wrap">
                                <a href="<?php echo $product['href']; ?>" class="product-link">Перейти к товару</a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
    </section>
</div>
<script>
    $('.owl-carousel_latest_home').owlCarousel_2({
        loop: true,
        nav: true,
        pagination: false,
        navText: ['<img src="/image/stril_mob_light.png" />', '<img src="/image/stril_mob_right.png" />'],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            768: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    })
</script>
