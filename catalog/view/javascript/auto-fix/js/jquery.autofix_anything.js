$(document).ready(function() {
  var fix_top = $('.fix_top').offset().top;
  $(window).on('scroll , resize, load', function() {
    var columnLeft = $('.fix_top').innerWidth(),
        headerHeight = $('.header').innerHeight(),
        footerHeight = $('.footer').innerHeight(),
        topMenu = $('.fix_top').offset().top - $(window).scrollTop(),
        topMenuWrap = $('.fix_top').offset().top - $(window).scrollTop(),
        footerPosition = $('.footer').offset().top - $(window).scrollTop() - fix_top,
        wHeight = window.innerHeight;

    if(($(window).scrollTop() - fix_top) >= topMenuWrap && window.innerWidth >= 600) {
      $('.fix_top').addClass('autofix_sb');
      $('.fix_top').css({
        'width': columnLeft,
        'max-height': 50,
        // 'height': 100 + '%',
        'overflow-y': 'hidden'
      });

      if($('.fix_top').height() >= footerPosition ){
        $('.fix_top').css({
          'max-height': $('.footer').offset().top - $(window).scrollTop()
        });
      }
    }
    else{
      $('.fix_top').removeClass('autofix_sb');
      $('.fix_top').css({
        'width': 100+'%',
        'overflow-y': 'hidden',
        'height': 50
      });
    }
  });
})