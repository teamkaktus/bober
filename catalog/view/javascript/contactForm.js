$(document).ready(function () {
    $('#send').on('click', function (even) {
        even.preventDefault();
        if (($('#phone').val().length < 3) && ($('#name').val().length < 3)){
            $('.phone').show();
            $('.name').show();
        /*} else if ($('#name').val().length < 3) {
            $('.name').show();
            $('.phone').hide();*/
        } else if ($('#phone').val().length < 3) {
            $('.phone').show();
            $('.name').hide()
        } else {
            $('.name').hide()
            $('.phone').hide();
            var res = $('#contactForm_c').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            swal("Сообщение отправлено", "", "success");
            $('#contactForm').removeClass('in');
            $('.modal-backdrop').removeClass('in');
            $.ajax({
                url: 'index.php?route=common/header/contactForm',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });
            $('#name').val('');
            $('#phone').val('');
        }
    });
});
