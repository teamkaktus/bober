<?php
class ControllerModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('module/featured');

		$this->document->addScript("/catalog/view/javascript/Feturet_slider/js/jquery.flexslider.js");
		$this->document->addScript("/catalog/view/javascript/Feturet_slider/js/jquery.easing.js");
		$this->document->addScript("/catalog/view/javascript/Feturet_slider/js/jquery.mousewheel.js");
		$this->document->addStyle("/catalog/view/javascript/Feturet_slider/css/flexslider.css");


		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}
					if ($this->config->get('length')) {
						$length = number_format($product_info['length'], 2, '.', '');
					} else {
						$length = false;
					}
					if ($this->config->get('width')) {
						$width = number_format($product_info['width'], 2, '.', '');
					} else {
						$width = false;
					}
					if ($this->config->get('height')) {
						$height= number_format($product_info['height'], 2, '.', '');
					} else {
						$height = false;
					}

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'manufacturer' => $product_info['manufacturer'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
						'price' => number_format(round($price, (int)$this->currency->getDecimalPlace()), (int)$this->currency->getDecimalPlace(), '.', ' '),
						'special' => number_format(round($special, (int)$this->currency->getDecimalPlace()), (int)$this->currency->getDecimalPlace(), '.', ' '),
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
					'length' 	  =>number_format(round($product_info['length'], (int)$this->currency->getDecimalPlace()), (int)$this->currency->getDecimalPlace(), '.', ''),
					'width' 	  =>number_format(round($product_info['width'], (int)$this->currency->getDecimalPlace()), (int)$this->currency->getDecimalPlace(), '.', ''),
					'height' 	  =>number_format(round($product_info['height'] , (int)$this->currency->getDecimalPlace()), (int)$this->currency->getDecimalPlace(), '.', ''),
					);
				}
			}
		}

		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);
			} else {
				return $this->load->view('default/template/module/featured.tpl', $data);
			}
		}
	}
}