<?php
class ControllerModuleCategoryHome extends Controller {
	public function index() {
		$this->load->language('module/category_home');

		$data['heading_title'] = $this->language->get('heading_title');

		$this->document->addScript("/catalog/view/javascript/owl.carousel.2/owl.carousel.js");
		$this->document->addStyle("/catalog/view/javascript/owl.carousel.2/assets/owl.carousel_category_home.css");

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			$children_data = array();

			if ($category['category_id'] == $data['category_id']) {
				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach($children as $child) {
					$filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

					$children_data[] = array(
						'category_id' => $child['category_id'],
						'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}
			}

			$filter_data = array(
				'filter_category_id'  => $category['category_id'],
				'filter_sub_category' => true
			);
			$this->load->model('tool/image');
			$data['categories'][] = array(
				'category_id' => $category['category_id'],
                'image'       => $this->model_tool_image->resize($category['image'], 138,104),
                'image2'       => $this->model_tool_image->resize($category['image2'], 138,104),
				'name'        => $category['name'],
				'children'    => $children_data,
				'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category_home.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/category_home.tpl', $data);
		} else {
			return $this->load->view('default/template/module/category_home.tpl', $data);
		}
	}
}