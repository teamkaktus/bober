<?php
class ControllerModuleBestSeller extends Controller {
	public function index($setting) {
		$this->load->language('module/bestseller');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['name'] =$setting['name'];
		$data['text'] =$setting['text'];

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);

		if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('length')) {
					$length = number_format($result['length'], 2, '.', '');
				} else {
					$length = false;
				}
				if ($this->config->get('width')) {
					$width = number_format($result['width'], 2, '.', '');
				} else {
					$width = false;
				}
				if ($this->config->get('height')) {
					$height= number_format($result['height'], 2, '.', '');
				} else {
					$height = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
				$icon = '';
				switch ($result['icon']) {
					case 1:{
						$icon = '/image/icon_hit.png';
						break;
					}
					case 2:{
						$icon = '/image/icon_prozent.png';
						break;
					}
					case 3:{
						$icon = '/image/icon_new.png';
						break;
					}
					case 0:{
						$icon = '';
						break;
					}
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'icon'        => $icon,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
					'length' 	  => number_format(round($result['length'], (int)$this->currency->getDecimalPlace()), (int)$this->currency->getDecimalPlace(), '.', ''),
					'width' 	  => number_format(round($result['width'], (int)$this->currency->getDecimalPlace()), (int)$this->currency->getDecimalPlace(), '.', ''),
					'height' 	  => number_format(round($result['height'] , (int)$this->currency->getDecimalPlace()), (int)$this->currency->getDecimalPlace(), '.', ''),
				);
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/bestseller.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/bestseller.tpl', $data);
			} else {
				return $this->load->view('default/template/module/bestseller.tpl', $data);
			}
		}
	}
}
